import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DepartmentModule } from './department/department.module';
import { Department } from './typeorm/entities/Department';
import { BlogModule } from './blog/blog.module';
import { Blog } from './typeorm/entities/Blog';
import { UserModule } from './user/user.module';
import { User } from './typeorm/entities/User';
import { ConfigModule } from '@nestjs/config';
import { ProductModule } from './product/product.module';
import { WorkshopModule } from './workshop/workshop.module';
import { Product } from './typeorm/entities/Product';
import { ProductType } from './typeorm/entities/Product_type';
import { ProductDietCategory } from './typeorm/entities/Product_diet_category';
import { JwtModule } from '@nestjs/jwt';
import { MailerModule } from '@nestjs-modules/mailer';
import { Gallery } from './typeorm/entities/Gallery';
import { Merchant } from './typeorm/entities/Merchant';
import { MerchantModule } from './merchant/merchant.module';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { MulterModule } from '@nestjs/platform-express';
import { join, extname } from 'path';
import { diskStorage } from 'multer';
import { GalleryModule } from './gallery/gallery.module';
import { WorshopGallery } from './typeorm/entities/Worshop-Gallery';
import { Workshop } from './typeorm/entities/Workshop';
import { ReservationDate } from './typeorm/entities/Reservation-Date';
import { Booking } from './typeorm/entities/Booking';
import { BookingModule } from './booking/booking.module';
import { ReservationDateModule } from './reservation-date/reservation-date.module';
import { Review } from './typeorm/entities/Review';
import { MerchantReview } from './typeorm/entities/MerchantReview';
import { Step } from './typeorm/entities/Step';
import { Itinerary } from './typeorm/entities/Itinerary';
import { Circuit } from './typeorm/entities/Circuit';
import { OpeningHours } from './typeorm/entities/Opening-Hours';
import { ReviewModule } from './review/review.module';
import { CircuitModule } from './circuit/circuit.module';
import { Wishlist } from './typeorm/entities/Wishlist';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DATABASE_HOST,
      port: process.env.NODE_ENV === 'production' ? undefined : 3306,
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_NAME,
      entities: [
        Department, 
        Blog, 
        User, 
        Product, 
        ProductType, 
        ProductDietCategory, 
        Gallery, Merchant, 
        WorshopGallery, 
        Workshop, 
        ReservationDate, 
        Booking,
        Review,
        MerchantReview,
        Step,
        Itinerary,
        Circuit,
        OpeningHours,
        Wishlist
      ],
      synchronize: true
    }),
    JwtModule.register({
      global: true,
      secret: `${process.env.SECRET_KEY}`,
      signOptions: { expiresIn: '1d' },
    }),
    MailerModule.forRoot({
      transport: {
        host: 'smtp.gmail.com',
        auth: {
          user: 'lavireeterroirofficiel@gmail.com',
          pass: 'nfiy creb xtbi ptrd',
        },
      },
      template: {
        dir: join(__dirname, 'mailer_template'),
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true,
        },
      },
    }),
    MulterModule.register({
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, cb) => {
          const originalName = file.originalname.split('.')[0];
          const fileExtName = extname(file.originalname);
          cb(null, `${originalName}-${Date.now()}${fileExtName}`);
        },
      }),
    }),
    DepartmentModule,
    BlogModule,
    UserModule,
    ProductModule,
    WorkshopModule,
    MerchantModule,
    GalleryModule,
    BookingModule,
    ReservationDateModule,
    ReviewModule,
    CircuitModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
