import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { Itinerary } from "./Itinerary";

@Entity({ name: 'Step' })
export class Step {
    @PrimaryGeneratedColumn()
    ID_step: number;

    @ApiProperty()
    @Column()
    step_name: string;

    @ApiProperty()
    @Column({ type: 'text' })
    step_description: string;

    @ApiProperty()
    @Column()
    step_order: number;

    @ManyToOne(() => Itinerary, itinerary => itinerary.steps)
    itinerary: Itinerary;
}
