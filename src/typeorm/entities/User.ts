import { Column, Entity, JoinColumn, JoinTable, ManyToMany, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { Merchant } from "./Merchant";
import { Booking } from "./Booking";
import { Review } from "./Review";
import { MerchantReview } from "./MerchantReview";
import { Circuit } from "./Circuit";
import { Wishlist } from "./Wishlist";

@Entity({ name: 'User' })
export class User {
    @PrimaryGeneratedColumn()
    ID_user: number;

    @ApiProperty()
    @Column()
    user_name: string;

    @ApiProperty()
    @Column()
    user_mail: string;

    @ApiProperty()
    @Column({ default: 'https://i.ibb.co/3YG2XmX/avatar.png' })
    avatar: string;

    @ApiProperty()
    @Column({ default: () => 'CURRENT_TIMESTAMP' })
    date_inscription: Date;

    @ApiProperty()
    @Column({ type: 'text', default: null })
    biography: string;

    @ApiProperty()
    @Column({default: null})
    firstname: string;

    @ApiProperty()
    @Column({default: null})
    lastname: string;

    @ApiProperty()
    @Column({default: null})
    user_phone: string;

    @ApiProperty()
    @Column({default: null})
    user_address: string;

    @ApiProperty()
    @Column({default: null})
    year: number;

    @ApiProperty()
    @Column()
    password: string;

    @OneToOne(() => Merchant, merchant => merchant.owner, { cascade: true })
    @JoinColumn()
    merchant: Merchant;

    @OneToMany(() => Booking, booking => booking.user)
    bookings: Booking[];

    @OneToMany(() => Review, review => review.user)
    reviews: Review[];

    @OneToMany(() => MerchantReview, merchantReview => merchantReview.user)
    merchantReviews: MerchantReview[];

    @ManyToMany(() => Circuit)
    @JoinTable()
    circuits: Circuit[];

    @OneToMany(() => Wishlist, wishlist => wishlist.user)
    wishlists: Wishlist[];

    @ApiProperty()
    @Column({default: false})
    admin: boolean;
}