import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, JoinColumn, ManyToMany } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { Department } from "./Department";
import { Itinerary } from "./Itinerary";
import { User } from "./User";

@Entity({ name: 'Circuit' })
export class Circuit {
    @PrimaryGeneratedColumn()
    ID_circuit: number;

    @ApiProperty()
    @Column()
    circuit_name: string;

    @ApiProperty()
    @Column()
    circuit_img: string;

    @ApiProperty()
    @Column({ type: 'text' })
    circuit_description: string;

    @ManyToOne(() => Department, department => department.circuits)
    department: Department;

    @ManyToMany(() => User, user => user.circuits)
    users: User[];

    @OneToOne(() => Itinerary, { cascade: true })
    @JoinColumn()
    itinerary: Itinerary;
}
