import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { Merchant } from "./Merchant";
import { DayOfWeek } from "src/typeorm/enum/day-of-week.enum";

@Entity({ name: 'OpeningHours' })
export class OpeningHours {
    @PrimaryGeneratedColumn()
    ID_opening_hours: number;

    @ApiProperty({ enum: DayOfWeek })
    @Column({ type: 'enum', enum: DayOfWeek })
    day_of_week: DayOfWeek;

    @ApiProperty()
    @Column({ type: 'time' })
    opening_time: string;

    @ApiProperty()
    @Column({ type: 'time' })
    closing_time: string;

    @ManyToOne(() => Merchant, merchant => merchant.openingHours)
    merchant: Merchant;
}
