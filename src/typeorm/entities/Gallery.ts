import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { Merchant } from "./Merchant";

@Entity({ name: 'Gallery' })
export class Gallery {
    @PrimaryGeneratedColumn()
    ID_photo: number;

    @ApiProperty()
    @Column({type: 'text'})
    url: string;

    @ManyToOne(() => Merchant, merchant => merchant.photos, { onDelete: 'CASCADE' })
    merchant: Merchant;
}