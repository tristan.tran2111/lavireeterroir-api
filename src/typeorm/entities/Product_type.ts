import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";

@Entity({ name: 'Product_type' })
export class ProductType {
    @PrimaryGeneratedColumn()
    ID_product_type: number;

    @ApiProperty()
    @Column()
    product_type_name: string;
}