import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { Product } from "./Product";
import { Merchant } from "./Merchant";
import { Circuit } from "./Circuit";

@Entity({ name: 'Department' })
export class Department {
    @PrimaryGeneratedColumn()
    ID_department: number;

    @ApiProperty()
    @Column()
    department_name: string;

    @ApiProperty()
    @Column({ type: 'text' })
    department_description: string;

    @ApiProperty()
    @Column({ type: 'text' })
    department_img: string;

    @ApiProperty()
    @Column()
    department_code: number;

    @ManyToMany(() => Product, (product) => product.departments)
    @JoinTable()
    products: Product[];

    @OneToMany(() => Merchant, merchant => merchant.department)
    merchants: Merchant[];

    @OneToMany(() => Circuit, circuit => circuit.department)
    circuits: Circuit[];
}
