import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, CreateDateColumn } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { User } from "./User";
import { Merchant } from "./Merchant";

@Entity({ name: 'MerchantReview' })
export class MerchantReview {
    @PrimaryGeneratedColumn()
    ID_review: number;

    @ApiProperty()
    @CreateDateColumn()
    review_date: Date;

    @ApiProperty()
    @Column({ type: 'int', default: 0 })
    rating: number;

    @ApiProperty()
    @Column({ type: 'text' })
    content: string;

    @ManyToOne(() => User, user => user.merchantReviews)
    user: User;

    @ManyToOne(() => Merchant, merchant => merchant.reviews)
    merchant: Merchant;
}
