import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { Workshop } from "./Workshop";
import { Booking } from "./Booking";

@Entity({ name: 'ReservationDate' })
export class ReservationDate {
    @PrimaryGeneratedColumn()
    ID_reservation_date: number;

    @ApiProperty()
    @Column({ type: 'date' })
    reservation_date: Date;

    @ApiProperty()
    @Column({ type: 'time' })
    start_time: Date;

    @ApiProperty()
    @Column({ type: 'time' })
    end_time: Date;

    @ApiProperty()
    @Column({ type: 'int' })
    available_places: number;

    @ManyToOne(() => Workshop, workshop => workshop.reservationDates)
    workshop: Workshop;

    @OneToMany(() => Booking, booking => booking.reservationDate)
    bookings: Booking[];
}
