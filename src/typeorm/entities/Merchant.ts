import { Column, Entity, PrimaryGeneratedColumn, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinTable } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { User } from "./User";
import { Gallery } from "./Gallery";
import { Department } from "./Department";
import { Product } from "./Product";
import { Workshop } from "./Workshop";
import { MerchantReview } from "./MerchantReview";
import { OpeningHours } from "./Opening-Hours";

@Entity({ name: 'Merchant' })
export class Merchant {
    @PrimaryGeneratedColumn()
    ID_merchant: number;

    @ApiProperty()
    @Column()
    merchant_name: string;

    @ApiProperty()
    @Column()
    merchant_address: string;

    @ApiProperty()
    @Column({type: 'text', nullable: true})
    merchant_description: string;

    @ApiProperty()
    @Column()
    merchant_email: string;

    @ApiProperty()
    @Column()
    merchant_phone: string;

    @OneToOne(() => User, user => user.merchant)
    owner: User;

    @OneToMany(() => Gallery, gallery => gallery.merchant)
    photos: Gallery[];

    @ManyToOne(() => Department, department => department.merchants)
    department: Department;

    @ManyToMany(() => Product, product => product.merchants)
    @JoinTable()
    products: Product[];
    
    @OneToMany(() => Workshop, workshop => workshop.organizer)
    workshops: Workshop[];

    @OneToMany(() => MerchantReview, review => review.merchant)
    reviews: MerchantReview[];

    @OneToMany(() => OpeningHours, openingHours => openingHours.merchant)
    openingHours: OpeningHours[];
}
