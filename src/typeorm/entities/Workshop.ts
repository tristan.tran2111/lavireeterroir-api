import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, OneToMany } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { Merchant } from "./Merchant";
import { WorshopGallery } from "./Worshop-Gallery";
import { Booking } from "./Booking";
import { ReservationDate } from "./Reservation-Date";
import { Review } from "./Review";

@Entity({ name: 'Workshop' })
export class Workshop {
    @PrimaryGeneratedColumn()
    ID_workshop: number;

    @ApiProperty()
    @Column()
    workshop_name: string;

    @ApiProperty()
    @Column()
    workshop_address: string;

    @ApiProperty()
    @Column({ type: 'text', nullable: true })
    workshop_description: string;

    @ApiProperty()
    @Column()
    workshop_time: number;

    @ApiProperty()
    @Column()
    workshop_person: number;

    @ApiProperty()
    @Column()
    workshop_year: number;

    @ApiProperty()
    @Column({ type: 'decimal' })
    workshop_price: number;

    @OneToMany(() => WorshopGallery, gallery => gallery.workshop)
    photos: WorshopGallery[];

    @ManyToOne(() => Merchant, merchant => merchant.workshops)
    organizer: Merchant;

    @OneToMany(() => Booking, booking => booking.workshop)
    bookings: Booking[];

    @OneToMany(() => ReservationDate, reservationDate => reservationDate.workshop)
    reservationDates: ReservationDate[];

    @OneToMany(() => Review, review => review.workshop)
    reviews: Review[];
}
