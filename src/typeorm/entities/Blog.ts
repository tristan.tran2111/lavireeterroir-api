import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { Department } from "./Department";

@Entity({ name: 'Blog' })
export class Blog {
    @PrimaryGeneratedColumn()
    ID_article: number;

    @ApiProperty()
    @Column()
    article_title: string;

    @ApiProperty()
    @Column({ type: 'longtext' })
    article_content: string;

    @ApiProperty()
    @Column({ type: 'text' })
    article_image: string;

    @ManyToOne(() => Department)
    @JoinColumn({ name: "ID_Department" })
    department: Department;
}