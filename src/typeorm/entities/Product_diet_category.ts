import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";

@Entity({ name: 'Product_diet_category' })
export class ProductDietCategory {
    @PrimaryGeneratedColumn()
    ID_product_diet_category: number;

    @ApiProperty()
    @Column()
    product_diet_category_name: string;
}