import { Column, Entity, JoinColumn, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { ProductType } from "./Product_type";
import { ProductDietCategory } from "./Product_diet_category";
import { Department } from "./Department";
import { Merchant } from "./Merchant";
import { Wishlist } from "./Wishlist";

@Entity({ name: 'Product' })
export class Product {
    @PrimaryGeneratedColumn()
    ID_product: number;

    @ApiProperty()
    @Column()
    product_name: string;

    @ApiProperty()
    @Column({ type: 'text' })
    product_description: string;

    @ApiProperty()
    @Column()
    product_img: string;

    @ManyToOne(() => ProductType)
    @JoinColumn({ name: "ID_product_type" })
    ProductType: ProductType;

    @ManyToOne(() => ProductDietCategory)
    @JoinColumn({ name: "ID_product_diet_category" })
    ProductDietCategory: ProductDietCategory;

    @ManyToMany(() => Department, (department) => department.products)
    departments: Department[];

    @ManyToMany(() => Merchant, (merchant) => merchant.products)
    merchants: Merchant[];

    @OneToMany(() => Wishlist, wishlist => wishlist.product)
    wishlist: Wishlist[];
}