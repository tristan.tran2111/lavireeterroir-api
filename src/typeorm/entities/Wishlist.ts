import { Entity, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { User } from "./User";
import { Product } from "./Product";

@Entity({ name: 'Wishlist' })
export class Wishlist {
    @PrimaryGeneratedColumn()
    ID_wishlist: number;

    @ApiProperty()
    @ManyToOne(() => User, user => user.wishlists)
    user: User;

    @ApiProperty()
    @ManyToOne(() => Product, product => product.wishlist)
    product: Product;
}