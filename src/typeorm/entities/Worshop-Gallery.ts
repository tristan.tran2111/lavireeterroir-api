import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { Workshop } from "./Workshop";

@Entity({ name: 'WorshopGallery' })
export class WorshopGallery {
    @PrimaryGeneratedColumn()
    ID_photo: number;

    @ApiProperty()
    @Column({type: 'text'})
    url: string;

    @ManyToOne(() => Workshop, workshop => workshop.photos, { onDelete: 'CASCADE' })
    workshop: Workshop;
}
