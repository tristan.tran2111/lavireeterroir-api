import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, CreateDateColumn } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { User } from "./User";
import { Workshop } from "./Workshop";

@Entity({ name: 'Review' })
export class Review {
    @PrimaryGeneratedColumn()
    ID_review: number;

    @ApiProperty()
    @CreateDateColumn()
    review_date: Date;

    @ApiProperty()
    @Column({ type: 'int', default: 0 })
    rating: number;

    @ApiProperty()
    @Column({ type: 'text' })
    content: string;

    @ManyToOne(() => User, user => user.reviews)
    user: User;

    @ManyToOne(() => Workshop, workshop => workshop.reviews)
    workshop: Workshop;
}
