import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { User } from "./User";
import { Workshop } from "./Workshop";
import { ReservationDate } from "./Reservation-Date";

@Entity({ name: 'Booking' })
export class Booking {
    @PrimaryGeneratedColumn()
    ID_booking: number;

    @ApiProperty()
    @Column({ type: 'date' })
    booking_date: Date;

    @ApiProperty()
    @Column({ type: 'decimal' })
    total_price: number;

    @ApiProperty()
    @Column({ type: 'int' })
    number_of_persons: number;

    @ManyToOne(() => User, user => user.bookings)
    user: User;

    @ManyToOne(() => Workshop, workshop => workshop.bookings)
    workshop: Workshop;

    @ManyToOne(() => ReservationDate, reservationDate => reservationDate.bookings)
    reservationDate: ReservationDate;
}
