import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { Step } from "./Step";
import { Circuit } from "./Circuit";

@Entity({ name: 'Itinerary' })
export class Itinerary {
    @PrimaryGeneratedColumn()
    ID_itinerary: number;

    @ApiProperty()
    @Column()
    itinerary_name: string;

    @OneToMany(() => Step, step => step.itinerary)
    steps: Step[];

    @ManyToOne(() => Circuit, circuit => circuit.itinerary)
    circuit: Circuit;
}
