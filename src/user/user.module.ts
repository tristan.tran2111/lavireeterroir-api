import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { User } from 'src/typeorm/entities/User';
import { Product } from 'src/typeorm/entities/Product';
import { Wishlist } from 'src/typeorm/entities/Wishlist';
import { Circuit } from 'src/typeorm/entities/Circuit';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Product, Wishlist, Circuit])
  ],
  providers: [UserService],
  controllers: [UserController]
})
export class UserModule {}
