import { BadRequestException, Body, Controller, Delete, Get, Param, Post, Put, Query, Res } from '@nestjs/common';
import { UserService } from './user.service';
import { ApiBadRequestResponse, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateUserDto } from './dtos/CreateUser.dto';
import { LoginUserDto } from './dtos/LoginUser.dto';
import { Response } from 'express'
import { User } from 'src/typeorm/entities/User';
import { UpdatePasswordDto } from './dtos/UpdatePassword.dto';
import { ForgotDto } from './dtos/Forgot.dto';
import { UpdateUserDto } from './dtos/UpdateUser.dto';

@ApiTags('User')
@Controller('user')
export class UserController {
    constructor(private userService: UserService) { }

    // For getting a user with his username
    @Get(':user_name/getUserByUsername')
    @ApiOperation({ summary: "Get a user" })
    @ApiOkResponse({ description: 'Display user by his username' })
    @ApiBadRequestResponse({ description: 'User was not found' })
    async getUserByUsername(@Param('user_name') user_name: string): Promise<User | BadRequestException> {
        return await this.userService.findUserByUsername(user_name);
    }

    // For send registering email
    @Post('signUp')
    @ApiCreatedResponse({
        description: 'Confirmation email sent'
    })
    @ApiBadRequestResponse({ description: 'Error sending mail' })
    async signup(@Body() createUserDto: CreateUserDto) {
        await this.userService.createUser(createUserDto);
        return { message: 'Confirmation email sent' };
    }

    @Get('bookings/:token')
    async getUserBookings(@Param('token') token: string) {
        return this.userService.getUserBookings(token);
    }

    // For send forgot email
    @Post('forgot')
    @ApiCreatedResponse({
        description: 'Forgot password email sent'
    })
    @ApiBadRequestResponse({ description: 'Error sending mail' })
    async forgot(@Body() forgotDto: ForgotDto) {
        await this.userService.sendForgotPasswordEmail(forgotDto.user_mail);
        return { message: 'Forgot password email sent' };
    }

    // For registering
    @Get('confirm/:token')
    async confirmUser(@Param('token') token: string) {
        return await this.userService.confirmUser(token);
    }

    // For login user
    @Post('signIn')
    @ApiCreatedResponse({
        description: 'User successfully connected'
    })
    @ApiBadRequestResponse({
        description: 'Error connecting user'
    })
    async signIn(@Res() res: Response, @Body() loginUserDto: LoginUserDto) {
        await this.userService.loginUser(loginUserDto.user_mail, loginUserDto.password, res);
        return { message: 'User successfully connected' };
    }

    // For reseting password of user
    @Put('updatePassword/:token')
    @ApiOkResponse({ description: 'User password updated successfully' })
    @ApiBadRequestResponse({ description: 'Error updating user password' })
    async updatePassword(@Param('token') token: string, @Body() updatePasswordDto: UpdatePasswordDto) {
        await this.userService.updateUserPassword(token, updatePasswordDto.password);
        return { message: 'User password updated successfully' };
    }

    // For checking integrity of the token
    @Get('validateToken')
    async validateToken(@Query('token') token: string): Promise<{ valid: boolean }> {
        const isValid = await this.userService.validateToken(token);
        return { valid: isValid };
    }

    // For checking if a user is a merchant
    @Get('isMerchant/:user_name')
    @ApiOperation({ summary: 'Check if the user is a merchant' })
    @ApiOkResponse({ description: 'Returns whether the user is a merchant' })
    @ApiBadRequestResponse({ description: 'User was not found' })
    async isMerchant(@Param('user_name') userName: string): Promise<{ valid: boolean }> {
        const isMerchant = await this.userService.isMerchant(userName);
        return { valid: isMerchant };
    }

    // Add product to wishlist
    @Post('wishlist/:productId')
    @ApiOperation({ summary: 'Add product to wishlist' })
    @ApiCreatedResponse({ description: 'Product added to wishlist' })
    @ApiBadRequestResponse({ description: 'Error adding product to wishlist' })
    async addToWishlist(@Query('token') token: string, @Param('productId') productId: number) {
        await this.userService.addToWishlist(token, productId);
        return { message: 'Product added to wishlist' };
    }

    // Get user wishlist
    @Get('wishlist')
    @ApiOperation({ summary: 'Get user wishlist' })
    @ApiOkResponse({ description: 'User wishlist retrieved' })
    @ApiBadRequestResponse({ description: 'Error retrieving wishlist' })
    async getWishlist(@Query('token') token: string) {
        return this.userService.getWishlist(token);
    }

    // Remove product from wishlist
    @Delete('wishlist/:productId')
    @ApiOperation({ summary: 'Remove product from wishlist' })
    @ApiOkResponse({ description: 'Product removed from wishlist' })
    @ApiBadRequestResponse({ description: 'Error removing product from wishlist' })
    async removeFromWishlist(@Query('token') token: string, @Param('productId') productId: number) {
        await this.userService.removeFromWishlist(token, productId);
        return { message: 'Product removed from wishlist' };
    }

    // Add circuit to user's followed circuits
    @Post('circuits/:circuitId')
    @ApiOperation({ summary: 'Add circuit to user\'s followed circuits' })
    @ApiCreatedResponse({ description: 'Circuit added to user\'s followed circuits' })
    @ApiBadRequestResponse({ description: 'Error adding circuit to user\'s followed circuits' })
    async addCircuitToUser(@Query('token') token: string, @Param('circuitId') circuitId: number) {
        await this.userService.addCircuitToUser(token, circuitId);
        return { message: 'Circuit added to user\'s followed circuits' };
    }

    // Get user's followed circuits
    @Get('circuits')
    @ApiOperation({ summary: 'Get user\'s followed circuits' })
    @ApiOkResponse({ description: 'User\'s followed circuits retrieved' })
    @ApiBadRequestResponse({ description: 'Error retrieving user\'s followed circuits' })
    async getUserCircuits(@Query('token') token: string) {
        return this.userService.getUserCircuits(token);
    }

    // Remove circuit from user's followed circuits
    @Delete('circuits/:circuitId')
    @ApiOperation({ summary: 'Remove circuit from user\'s followed circuits' })
    @ApiOkResponse({ description: 'Circuit removed from user\'s followed circuits' })
    @ApiBadRequestResponse({ description: 'Error removing circuit from user\'s followed circuits' })
    async removeCircuitFromUser(@Query('token') token: string, @Param('circuitId') circuitId: number) {
        await this.userService.removeCircuitFromUser(token, circuitId);
        return { message: 'Circuit removed from user\'s followed circuits' };
    }

    // For checking if a circuit is followed by the user
    @Get('circuits/:circuitId/is-followed')
    @ApiOperation({ summary: 'Check if circuit is followed by the user' })
    @ApiOkResponse({ description: 'Returns whether the circuit is followed by the user' })
    @ApiBadRequestResponse({ description: 'Error checking if circuit is followed by the user' })
    async isCircuitFollowedByUser(@Query('token') token: string, @Param('circuitId') circuitId: number): Promise<{ followed: boolean }> {
        const isFollowed = await this.userService.isCircuitFollowedByUser(token, circuitId);
        return { followed: isFollowed };
    }

    @Get('profile/:token')
    async getUserProfile(@Param('token') token: string) {
        return this.userService.getUserByToken(token);
    }

    @Put('updateProfile/:token')
  async updateUserProfile(@Param('token') token: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return await this.userService.updateUserProfile(token, updateUserDto);
  }
}
