import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/typeorm/entities/User';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { Response } from 'express';
import { MailerService } from '@nestjs-modules/mailer';
import { CreateUserDto } from './dtos/CreateUser.dto';
import { Wishlist } from 'src/typeorm/entities/Wishlist';
import { Product } from 'src/typeorm/entities/Product';
import { Circuit } from 'src/typeorm/entities/Circuit';
import { UpdateUserDto } from './dtos/UpdateUser.dto';

@Injectable()
export class UserService {
  private readonly usedTokens: Set<string> = new Set<string>();

  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    @InjectRepository(Product) private productRepository: Repository<Product>,
    @InjectRepository(Wishlist) private wishlistRepository: Repository<Wishlist>,
    @InjectRepository(Circuit) private circuitRepository: Repository<Circuit>,
    private jwtService: JwtService,
    private readonly mailerService: MailerService
  ) {
    this.scheduleClearUsedTokens();
  }

  // For @get(':user_name/getUserByUsername')
  async findUserByUsername(user_name: string) {
    try {
      return await this.userRepository.findOne({
        where: { user_name: user_name }
      });
    } catch (error) {
      console.error(error);
      throw new BadRequestException('Error fetching data of the user');
    }
  }

  async getUserByToken(token: string) {
    try {
      const decodedToken = this.jwtService.verify(token);
      const user = await this.userRepository.findOne({
        where: { ID_user: decodedToken.user_id }
      });
  
      if (!user) {
        throw new UnauthorizedException('Invalid token');
      }
  
      return user;
    } catch (error) {
      console.error(error);
      throw new BadRequestException('Error retrieving user');
    }
  }

  async getUserBookings(token: string) {
    try {
      const decodedToken = this.jwtService.verify(token);
      const user = await this.userRepository.findOne({
        where: { ID_user: decodedToken.user_id },
        relations: ['bookings', 'bookings.workshop', 'bookings.reservationDate', 'bookings.workshop.photos'],
      });

      if (!user) {
        throw new UnauthorizedException('Invalid token');
      }

      return user.bookings;
    } catch (error) {
      console.error(error);
      throw new BadRequestException('Error retrieving user bookings');
    }
  }

  // For @Post('signUp')
  async createUser(userDetails: CreateUserDto) {
    const existingUser = await this.findUserByUsername(userDetails.user_name);
    const existingEmailUser = await this.userRepository.findOne({ where: { user_mail: userDetails.user_mail } });

    if (existingUser) {
      throw new BadRequestException('Username already exists');
    } else if (existingEmailUser) {
      throw new BadRequestException('Email already used');
    } else {
      const token = this.jwtService.sign(userDetails, { expiresIn: '1h' });
      await this.mailerService.sendMail({
        to: userDetails.user_mail,
        subject: 'Confirmation inscription La Virée Terroir',
        template: './confirmation-template',
        context: {
          token: token,
          mailUrlToApp: process.env.MAIL_URL_TO_APP,
        },
      });
    }
  }

  // For @Get('confirm/:token')
  async confirmUser(token: string) {
    if (this.usedTokens.has(token)) {
      throw new BadRequestException('Token already used');
    } else {
      try {
        const userDetails = this.jwtService.verify(token);
        const existingUser = await this.findUserByUsername(userDetails.user_name);

        if (existingUser) {
          throw new BadRequestException('Username already exists');
        } else {

          const hashedPassword = bcrypt.hashSync(userDetails.password, 10);
          const newUser = this.userRepository.create({
            ...userDetails,
            password: hashedPassword
          });
          await this.userRepository.save(newUser);
          this.usedTokens.add(token);

          return { message: 'User successfully created', data: userDetails.user_name };
        }
      } catch (error) {
        console.error(error);
        throw new BadRequestException('Error creating user');
      }
    }
  }

  // For @Post('signIn')
  async loginUser(user_mail: string, password: string, res: Response): Promise<void> {
    try {
      const user = await this.userRepository.findOne({ where: { user_mail }, relations: ['merchant'] });

      if (!user) {
        throw new UnauthorizedException('Invalid credentials');
      }

      const isPasswordValid = await bcrypt.compare(password, user.password);

      if (!isPasswordValid) {
        throw new UnauthorizedException('Invalid credentials');
      }

      const userToken = this.jwtService.sign({ user_id: user.ID_user });

      let merchantToken: string = null;
      if (user.merchant) {
        merchantToken = this.jwtService.sign({ merchant_id: user.merchant.ID_merchant });
      }

      res.json({ token: userToken, merchantToken: merchantToken, data: user.user_name });

    } catch (error) {
      console.error(error);
      throw new BadRequestException('Error connecting user');
    }
  }

  // For @Get('validateToken')
  async validateToken(token: string): Promise<boolean> {
    try {
      const decoded = this.jwtService.verify(token);
      return !!decoded;
    } catch (error) {
      return false;
    }
  }

  // For @Get('isMerchant/:user_name')
  async isMerchant(userName: string): Promise<boolean> {
    const user = await this.userRepository.findOne({
      where: { user_name: userName },
      relations: ['merchant'],
    });

    if (!user) {
      throw new BadRequestException('User not found');
    }

    return !!user.merchant;
  }

  // @Put(':token/updatePassword')
  async updateUserPassword(token: string, password: string): Promise<void> {
    try {
      const decodedToken = this.jwtService.verify(token);
      const user_mail: string = decodedToken.user_mail;

      const user = await this.userRepository.findOne({ where: { user_mail } });

      if (!user) {
        throw new BadRequestException('User not found');
      }

      const hashedPassword = bcrypt.hashSync(password, 10);

      user.password = hashedPassword;
      await this.userRepository.save(user);
    } catch (error) {
      console.error(error);
      throw new BadRequestException('Error updating user password');
    }
  }


  // For  @Post('forgot')
  async sendForgotPasswordEmail(user_mail: string) {
    const user = await this.userRepository.findOne({ where: { user_mail } });

    if (!user) {
      throw new UnauthorizedException('Invalid credentials');
    } else {
      const userPayload = {
        user_mail: user.user_mail
      };
      const token = this.jwtService.sign(userPayload, { expiresIn: '1h' });
      await this.mailerService.sendMail({
        to: user_mail,
        subject: 'Réinitialisation du mot de passe',
        template: './forgot-password-template',
        context: {
          token: token,
          mailUrlToApp: process.env.MAIL_URL_TO_APP,
        },
      });
    }
  }

  // Add a product to the user's wishlist
  async addToWishlist(token: string, productId: number) {
    const decodedToken = this.jwtService.verify(token);
    const user = await this.userRepository.findOne({ where: { ID_user: decodedToken.user_id } });
    const product = await this.productRepository.findOne({ where: { ID_product: productId } });

    if (!user || !product) {
      throw new BadRequestException('User or Product not found');
    }

    const wishlistEntry = this.wishlistRepository.create({ user, product });
    await this.wishlistRepository.save(wishlistEntry);
  }

  // Get the user's wishlist
  async getWishlist(token: string) {
    const decodedToken = this.jwtService.verify(token);
    const wishlist = await this.wishlistRepository.find({
      where: { user: { ID_user: decodedToken.user_id } },
      relations: ['product'],
    });

    return wishlist.map(entry => entry.product);
  }

  // Remove a product from the user's wishlist
  async removeFromWishlist(token: string, productId: number) {
    const decodedToken = this.jwtService.verify(token);
    const wishlistEntry = await this.wishlistRepository.findOne({
      where: { user: { ID_user: decodedToken.user_id }, product: { ID_product: productId } },
    });

    if (!wishlistEntry) {
      throw new BadRequestException('Wishlist entry not found');
    }

    await this.wishlistRepository.remove(wishlistEntry);
  }

  // Add a circuit to the user's followed circuits
  async addCircuitToUser(token: string, circuitId: number) {
    const decodedToken = this.jwtService.verify(token);
    const user = await this.userRepository.findOne({ where: { ID_user: decodedToken.user_id }, relations: ['circuits'] });
    const circuit = await this.circuitRepository.findOne({ where: { ID_circuit: circuitId } });

    if (!user || !circuit) {
      throw new BadRequestException('User or Circuit not found');
    }

    user.circuits.push(circuit);
    await this.userRepository.save(user);
  }

  // Get the user's followed circuits
  async getUserCircuits(token: string) {
    const decodedToken = this.jwtService.verify(token);
    const user = await this.userRepository.findOne({
      where: { ID_user: decodedToken.user_id },
      relations: ['circuits'],
    });

    if (!user) {
      throw new BadRequestException('User not found');
    }

    return user.circuits;
  }

  // Remove a circuit from the user's followed circuits
  async removeCircuitFromUser(token: string, circuitId: number) {
    const decodedToken = this.jwtService.verify(token);
    const user = await this.userRepository.findOne({ where: { ID_user: decodedToken.user_id }, relations: ['circuits'] });

    if (!user) {
      throw new BadRequestException('User not found');
    }

    const circuit = await this.circuitRepository.findOne({ where: { ID_circuit: circuitId } });

    if (!circuit) {
      throw new BadRequestException('Circuit not found');
    }

    // Remove the relationship using QueryBuilder
    await this.userRepository.createQueryBuilder()
      .relation(User, "circuits")
      .of(user)
      .remove(circuit);
  }


  async isCircuitFollowedByUser(token: string, circuitId: number): Promise<boolean> {
    const decodedToken = this.jwtService.verify(token);
    const user = await this.userRepository.findOne({ where: { ID_user: decodedToken.user_id } });
  
    if (!user) {
      throw new BadRequestException('User not found');
    }
  
    const isFollowed = await this.userRepository.createQueryBuilder("user")
      .innerJoin("user.circuits", "circuit")
      .where("user.ID_user = :userId", { userId: user.ID_user })
      .andWhere("circuit.ID_circuit = :circuitId", { circuitId })
      .getCount();
  
    return isFollowed > 0;
  }

  async updateUserProfile(token: string, updateUserDto: UpdateUserDto) {
    try {
      const decodedToken = this.jwtService.verify(token);
      const user = await this.userRepository.findOne({
        where: { ID_user: decodedToken.user_id }
      });

      if (!user) {
        throw new UnauthorizedException('Invalid token');
      }

      // Mettre à jour les informations de l'utilisateur
      if (updateUserDto.user_name) {
        user.user_name = updateUserDto.user_name;
      }
      if (updateUserDto.user_mail) {
        const existingEmailUser = await this.userRepository.findOne({ where: { user_mail: updateUserDto.user_mail } });
        if (existingEmailUser && existingEmailUser.ID_user !== user.ID_user) {
          throw new BadRequestException('Email already used');
        }
        user.user_mail = updateUserDto.user_mail;
      }
      if (updateUserDto.biography) {
        user.biography = updateUserDto.biography;
      }
      if (updateUserDto.firstname) {
        user.firstname = updateUserDto.firstname;
      }
      if (updateUserDto.lastname) {
        user.lastname = updateUserDto.lastname;
      }

      if (updateUserDto.user_address) {
        user.user_address = updateUserDto.user_address;
      }

      if (updateUserDto.year) {
        user.year = updateUserDto.year;
      }

      await this.userRepository.save(user);
      return { message: 'User profile updated successfully', data: user };
    } catch (error) {
      console.error(error);
      throw new BadRequestException('Error updating user profile');
    }
  }

  // Reset Set data every day
  private scheduleClearUsedTokens() {
    const oneDayInMillis = 24 * 60 * 60 * 1000;
    const now = new Date();
    const tomorrow = new Date(now.getTime() + oneDayInMillis);
    tomorrow.setHours(0, 0, 0, 0);

    const timeUntilMidnight = tomorrow.getTime() - now.getTime();

    setTimeout(() => {
      this.clearUsedTokens();
      this.scheduleClearUsedTokens();
    }, timeUntilMidnight);
  }

  // Reset Set data
  private clearUsedTokens() {
    this.usedTokens.clear();
  }
}
