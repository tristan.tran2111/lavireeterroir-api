import { ApiProperty } from "@nestjs/swagger";

export class CreateUserDto {

    @ApiProperty()
    user_name: string;

    @ApiProperty()
    user_mail: string;

    @ApiProperty()
    avatar: string;

    @ApiProperty()
    biography: string;

    @ApiProperty()
    firstname: string;

    @ApiProperty()
    lastname: string;

    @ApiProperty()
    user_address: string;

    @ApiProperty()
    year: number;

    @ApiProperty()
    password: string;
}