import { ApiProperty } from "@nestjs/swagger";

export class ForgotDto {

    @ApiProperty()
    user_mail: string;

}