import { ApiProperty } from "@nestjs/swagger";

export class LoginUserDto {

    @ApiProperty()
    user_mail: string;

    @ApiProperty()
    password: string;

}