import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Blog } from '../typeorm/entities/Blog';
import { Repository } from 'typeorm';
import { Department } from '../typeorm/entities/Department';
import { CreateBlogDto } from './dtos/create-blog.dto';
import * as admin from 'firebase-admin';

@Injectable()
export class BlogService {
  constructor(
    @InjectRepository(Blog) private blogRepository: Repository<Blog>,
    @InjectRepository(Department) private departmentRepository: Repository<Department>,
  ) {}

  async findAllBlog() {
    return this.blogRepository.find({ relations: ['department'] });
  }

  async findBlogByName(article_title: string) {
    return this.blogRepository.findOne({
      where: { article_title },
      relations: ['department']
    });
  }

  findPaginatedBlogs(page: number, itemsPerPage: number) {
    const skip = (page - 1) * itemsPerPage;
    return this.blogRepository.find({
      relations: ['department'],
      skip,
      take: itemsPerPage,
    });
  }

  async createBlog(createBlogDto: CreateBlogDto, file: Express.Multer.File) {
    const department = await this.departmentRepository.findOne({ where: { ID_department: createBlogDto.ID_Department } });
    
    const imageUrl = await this.uploadFile(file);

    const blog = this.blogRepository.create({
      ...createBlogDto,
      article_image: imageUrl,
      department,
    });
    
    return this.blogRepository.save(blog);
  }

  async uploadFile(file: Express.Multer.File): Promise<string> {
    const bucket = admin.storage().bucket();
    const fileName = `${Date.now()}-${file.originalname}`;
    const fileUpload = bucket.file(fileName);
    const stream = fileUpload.createWriteStream({
      metadata: {
        contentType: file.mimetype,
      },
    });

    stream.end(file.buffer);

    await new Promise((resolve, reject) => {
      stream.on('finish', resolve);
      stream.on('error', reject);
    });

    const [url] = await fileUpload.getSignedUrl({
      action: 'read',
      expires: '03-01-2500',
    });

    return url;
  }

  async findBlogsByDepartmentCode(departmentCode: number) {
    return this.blogRepository.find({
      where: {
        department: { department_code: departmentCode },
      },
      relations: ['department'],
    });
  }
}
