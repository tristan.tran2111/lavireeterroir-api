import { Controller, Get, Post, Body, ParseIntPipe, Query, UploadedFile, UseInterceptors, Param } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { BlogService } from './blog.service';
import { ApiBadRequestResponse, ApiBody, ApiConsumes, ApiOkResponse, ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreateBlogDto, CreateBlogWithFileDto } from './dtos/create-blog.dto';

@ApiTags('Blog')
@Controller('blog')
export class BlogController {
  constructor(private readonly blogService: BlogService) {}

  @Get('getBlogs')
  @ApiOperation({ summary: "Get all blogs" })
  @ApiOkResponse({ description: 'Display all blogs of the app' })
  @ApiBadRequestResponse({ description: 'Blogs not found' })
  getBlogs() {
    return this.blogService.findAllBlog();
  }

  @Get('getPaginatedBlogs')
  @ApiOperation({ summary: "Get paginated blogs" })
  @ApiOkResponse({ description: 'Display paginated blogs of the app' })
  @ApiBadRequestResponse({ description: 'Blogs not found' })
  @ApiQuery({ name: 'page', type: Number })
  @ApiQuery({ name: 'itemsPerPage', type: Number })
  getPaginatedBlogs(
    @Query('page', ParseIntPipe) page: number,
    @Query('itemsPerPage', ParseIntPipe) itemsPerPage: number,
  ) {
    return this.blogService.findPaginatedBlogs(page, itemsPerPage);
  }

  @Post('createBlog')
  @UseInterceptors(FileInterceptor('article_image'))
  @ApiOperation({ summary: "Create a new blog" })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'Create Blog DTO',
    type: CreateBlogWithFileDto,
  })
  @ApiOkResponse({ description: 'Blog created successfully' })
  @ApiBadRequestResponse({ description: 'Invalid data' })
  createBlog(@Body() createBlogDto: CreateBlogDto, @UploadedFile() article_image: Express.Multer.File) {
    return this.blogService.createBlog(createBlogDto, article_image);
  }

  @Get('/:name')
  findBlogByName(@Param('name') name: string) {
    return this.blogService.findBlogByName(name);
  }

  @Get('department/:code')
  @ApiOperation({ summary: 'Get blogs by department code' })
  @ApiResponse({ status: 200, description: 'Return blogs by department code.' })
  findByDepartmentCode(@Param('code') code: number) {
    return this.blogService.findBlogsByDepartmentCode(code);
  }
}
