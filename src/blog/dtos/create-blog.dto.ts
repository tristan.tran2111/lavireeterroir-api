import { ApiProperty } from '@nestjs/swagger';

export class CreateBlogDto {
  @ApiProperty()
  article_title: string;

  @ApiProperty()
  article_content: string;

  @ApiProperty()
  ID_Department: number;
}

export class CreateBlogWithFileDto extends CreateBlogDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  article_image: any;
}
