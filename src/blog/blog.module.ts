import { Module } from '@nestjs/common';
import { BlogService } from './blog.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Blog } from 'src/typeorm/entities/Blog';
import { BlogController } from './blog.controller';
import { Department } from 'src/typeorm/entities/Department';

@Module({
  imports: [
    TypeOrmModule.forFeature([Blog, Department])
  ],
  providers: [BlogService],
  controllers: [BlogController]
})
export class BlogModule {}
