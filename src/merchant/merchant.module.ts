import { Module } from '@nestjs/common';
import { MerchantService } from './merchant.service';
import { MerchantController } from './merchant.controller';
import { Merchant } from 'src/typeorm/entities/Merchant';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/typeorm/entities/User';
import { Gallery } from 'src/typeorm/entities/Gallery';
import { GalleryService } from 'src/gallery/gallery.service';
import { Product } from 'src/typeorm/entities/Product';

@Module({
  imports: [
    TypeOrmModule.forFeature([Merchant, User, Gallery, Product])
  ],
  providers: [MerchantService, GalleryService],
  controllers: [MerchantController]
})
export class MerchantModule {}
