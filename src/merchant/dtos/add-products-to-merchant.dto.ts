// src/merchant/dtos/add-products-to-merchant.dto.ts
import { ApiProperty } from '@nestjs/swagger';

export class AddProductsToMerchantDto {
  @ApiProperty()
  productIds: number[];
}
