import { ApiProperty } from '@nestjs/swagger';

export class CreateMerchantDto {
  @ApiProperty()
  merchant_name: string;

  @ApiProperty()
  merchant_address: string;

  @ApiProperty()
  merchant_email: string;

  @ApiProperty()
  merchant_phone: string;
}
