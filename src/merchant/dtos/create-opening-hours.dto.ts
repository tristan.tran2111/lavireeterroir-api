import { ApiProperty } from "@nestjs/swagger";
import { DayOfWeek } from "src/typeorm/enum/day-of-week.enum";

export class CreateOpeningHoursDto {
  @ApiProperty({ enum: DayOfWeek })
  day_of_week: DayOfWeek;

  @ApiProperty()
  opening_time: string;

  @ApiProperty()
  closing_time: string;

  @ApiProperty()
  merchantId: number;
}
