import { Controller, Post, Body, Param, Get, UseInterceptors, UploadedFile, HttpException, HttpStatus, Delete } from '@nestjs/common';
import { MerchantService } from './merchant.service';
import { GalleryService } from '../gallery/gallery.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags, ApiOperation, ApiCreatedResponse, ApiBadRequestResponse, ApiConsumes, ApiBody } from '@nestjs/swagger';
import { CreateMerchantDto } from './dtos/create-merchant.dto';
import { JwtService } from '@nestjs/jwt';
import { Merchant } from 'src/typeorm/entities/Merchant';

@ApiTags('Merchant')
@Controller('merchants')
export class MerchantController {
  constructor(
    private readonly merchantService: MerchantService,
    private readonly galleryService: GalleryService,
    private readonly jwtService: JwtService
  ) {}

  @Get(':name')
  @ApiOperation({ summary: 'Get merchant by name' })
  @ApiCreatedResponse({ status: 200, description: 'Return merchant data.', type: Merchant })
  @ApiBadRequestResponse({ status: 404, description: 'Merchant not found.' })
  async getMerchantByName(@Param('name') name: string) {
    return this.merchantService.findMerchantByName(name);
  }

  @Post('sendMailCreateCommerce')
  @ApiOperation({ summary: 'Sending mail for creating a merchant from a user account' })
  @ApiCreatedResponse({ description: 'Mail successfully send' })
  @ApiBadRequestResponse({ description: 'Error sending mail' })
  async sendMailCreateMerchant(@Body() createMerchantDto: CreateMerchantDto) {
    await this.merchantService.sendMailcreateMerchant(createMerchantDto);
    return { message: 'Mail successfully send' };
  }

  @Get('confirm/:user_name/:token')
  async confirmCreateMerchant(@Param('user_name') user_name: string, @Param('token') token: string) {
    return await this.merchantService.createMerchant(user_name, token);
  }

  @Post('upload/:token')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(FileInterceptor('file'))
  @ApiOperation({ summary: 'Upload a file for a merchant' })
  @ApiCreatedResponse({ description: 'File successfully uploaded' })
  @ApiBadRequestResponse({ description: 'Error uploading file' })
  public async uploadFile(@Param('token') token: string, @UploadedFile() file: Express.Multer.File) {
    if (!file || !file.buffer) {
      console.error('File or file buffer is undefined');
      throw new HttpException('File not provided', HttpStatus.BAD_REQUEST);
    }

    let merchantId: number;
    try {
      const decoded = this.jwtService.verify(token);
      merchantId = decoded.merchant_id;
      if (!merchantId) {
        throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
      }
    } catch (error) {
      throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
    }

    console.log('Uploaded file:', file);

    try {
      const fileUrl = await this.merchantService.uploadFile(file);

      const merchant = await this.merchantService.findOne(merchantId);
      if (!merchant) {
        throw new HttpException('Merchant not found', HttpStatus.NOT_FOUND);
      }

      await this.galleryService.create(fileUrl, merchant);

      return { fileUrl };
    } catch (error) {
      throw error;
    }
  }

  @Get('getMerchantData/:token')
  @ApiOperation({ summary: 'Get all data for a merchant' })
  @ApiCreatedResponse({ description: 'Data successfully retrieved' })
  @ApiBadRequestResponse({ description: 'Error retrieving data' })
  public async getMerchantData(@Param('token') token: string) {
    let merchantId: number;
    try {
      const decoded = this.jwtService.verify(token);
      merchantId = decoded.merchant_id;
      if (!merchantId) {
        throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
      }
    } catch (error) {
      throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
    }

    const merchant = await this.merchantService.findOne(merchantId);
    if (!merchant) {
      throw new HttpException('Merchant not found', HttpStatus.NOT_FOUND);
    }

    return { merchant };
  }

  @Delete('deleteUpload/:token/:id')
  @ApiOperation({ summary: 'Delete an uploaded file for a merchant' })
  @ApiCreatedResponse({ description: 'File successfully deleted' })
  @ApiBadRequestResponse({ description: 'Error deleting file' })
  public async deleteUpload(@Param('token') token: string, @Param('id') id: number) {
    await this.merchantService.deleteUpload(token, id);
    return { message: 'File successfully deleted' };
  }
}
