import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Merchant } from '../typeorm/entities/Merchant';
import { User } from '../typeorm/entities/User';
import { JwtService } from '@nestjs/jwt';
import * as admin from 'firebase-admin';
import { ServiceAccount } from 'firebase-admin';
import { MailerService } from '@nestjs-modules/mailer';
import { CreateMerchantDto } from './dtos/create-merchant.dto';
import { Gallery } from '../typeorm/entities/Gallery';
import * as path from 'path';

@Injectable()
export class MerchantService {
  private readonly usedTokens: Set<string> = new Set<string>();

  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    @InjectRepository(Merchant) private merchantRepository: Repository<Merchant>,
    @InjectRepository(Gallery) private galleryRepository: Repository<Gallery>,
    private jwtService: JwtService,
    private readonly mailerService: MailerService
  ) {
    this.scheduleClearUsedTokens();

    const serviceAccountPath = path.join(__dirname, '..', 'lavireeterroir-firebase-adminsdk-h9x7f-7e7a584f59.json');
    
    if (!serviceAccountPath) {
      throw new Error('Firebase service account key file path is not set');
    }

    const serviceAccount: ServiceAccount = require(serviceAccountPath);
    admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
      storageBucket: 'lavireeterroir.appspot.com',
    });
  }

  async findOne(id: number): Promise<Merchant> {
    return this.merchantRepository.findOne({
      where: { ID_merchant: id },
      relations: ['photos', 'owner', 'products', 'workshops', 'workshops.photos', 'reviews', 'reviews.user', 'openingHours'],
    });
  }

  async findMerchantByName(merchant_name: string) {
    return this.merchantRepository.findOne({
      where: { merchant_name },
      relations: ['photos', 'owner', 'products', 'workshops', 'workshops.photos', 'reviews', 'reviews.user', 'openingHours']
    });
  }

  async sendMailcreateMerchant(createMerchantDetails: CreateMerchantDto) {
    const token = this.jwtService.sign(createMerchantDetails, { expiresIn: '1h' });
    await this.mailerService.sendMail({
      to: createMerchantDetails.merchant_email,
      subject: 'Confirmation inscription commerce La Virée Terroir',
      template: './confirmation-merchant-template',
        context: {
          token: token,
          mailUrlToApp: process.env.MAIL_URL_TO_APP,
        },
    });
  }

  async createMerchant(user_name: string, token: string) {
    if (this.usedTokens.has(token)) {
      throw new BadRequestException('Token already used');
    } else {
      try {
        const user = await this.userRepository.findOne({
          where: { user_name: user_name }
        });
        const commerceDetails = this.jwtService.verify(token);

        const newMerchant = this.merchantRepository.create({
          ...commerceDetails,
          owner: user,
        });

        await this.merchantRepository.save(newMerchant);
        this.usedTokens.add(token);

        return { message: 'Merchant successfully created', data: commerceDetails };
      } catch (error) {
        console.error(error);
        throw new BadRequestException('Error creating Merchant');
      }
    }
  }

  async uploadFile(file: Express.Multer.File): Promise<string> {
    const bucket = admin.storage().bucket();
    const fileName = `${Date.now()}-${file.originalname}`;
    const fileUpload = bucket.file(fileName);
    const stream = fileUpload.createWriteStream({
      metadata: {
        contentType: file.mimetype,
      },
    });

    stream.end(file.buffer);

    await new Promise((resolve, reject) => {
      stream.on('finish', resolve);
      stream.on('error', reject);
    });

    const [url] = await fileUpload.getSignedUrl({
      action: 'read',
      expires: '03-01-2500',
    });

    return url;
  }

  async deleteUpload(token: string, fileId: number): Promise<void> {
    let merchantId: number;
    try {
      const decoded = this.jwtService.verify(token);
      merchantId = decoded.merchant_id;
    } catch (error) {
      throw new BadRequestException('Invalid token');
    }

    const gallery = await this.galleryRepository.findOne({ where: { ID_photo: fileId, merchant: { ID_merchant: merchantId } } });
    if (!gallery) {
      throw new BadRequestException('Gallery item not found');
    }

    const bucket = admin.storage().bucket();
    const url = new URL(gallery.url);
    const fileName = url.pathname.split('/').pop();
    if (!fileName) {
      throw new BadRequestException('Invalid file URL');
    }

    const file = bucket.file(fileName);
    await file.delete();

    await this.galleryRepository.delete(gallery.ID_photo);
  }

  // Reset Set data every day
  private scheduleClearUsedTokens() {
    const oneDayInMillis = 24 * 60 * 60 * 1000;
    const now = new Date();
    const tomorrow = new Date(now.getTime() + oneDayInMillis);
    tomorrow.setHours(0, 0, 0, 0);

    const timeUntilMidnight = tomorrow.getTime() - now.getTime();

    setTimeout(() => {
      this.clearUsedTokens();
      this.scheduleClearUsedTokens();
    }, timeUntilMidnight);
  }

  // Reset Set data
  private clearUsedTokens() {
    this.usedTokens.clear();
  }
}
