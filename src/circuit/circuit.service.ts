import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Circuit } from 'src/typeorm/entities/Circuit';
import { Repository } from 'typeorm';

@Injectable()
export class CircuitService {

  constructor(
    @InjectRepository(Circuit) private circuitRepository: Repository<Circuit>,
  ) {}
  async findCircuitByDepartmentCode(departmentCode: number) {
    return this.circuitRepository.find({
      where: {
        department: { department_code: departmentCode },
      },
      relations: ['department'],
    });
  }

  async findCircuitById(id: number) {
    return this.circuitRepository.findOne({
      where: { ID_circuit: id },
      relations: ['itinerary', 'itinerary.steps']
    });
  }
}
