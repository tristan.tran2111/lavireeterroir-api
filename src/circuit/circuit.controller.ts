import { Controller, Get, Param } from '@nestjs/common';
import { CircuitService } from './circuit.service';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Circuit')
@Controller('circuit')
export class CircuitController {
  constructor(private readonly circuitService: CircuitService) {}

  @Get('department/:code')
  @ApiOperation({ summary: 'Get circuits by department code' })
  @ApiResponse({ status: 200, description: 'Return circuits by department code.' })
  findByDepartmentCode(@Param('code') code: number) {
    return this.circuitService.findCircuitByDepartmentCode(code);
  }

  @Get(':id')
  @ApiOperation({ summary: 'Get circuit by ID' })
  @ApiResponse({ status: 200, description: 'Return circuit by ID.' })
  findById(@Param('id') id: number) {
    return this.circuitService.findCircuitById(id);
  }
}
