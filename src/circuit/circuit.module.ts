import { Module } from '@nestjs/common';
import { CircuitService } from './circuit.service';
import { CircuitController } from './circuit.controller';
import { Circuit } from 'src/typeorm/entities/Circuit';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([Circuit])
  ],
  providers: [CircuitService],
  controllers: [CircuitController]
})
export class CircuitModule {}
