import { Module } from '@nestjs/common';
import { ProductService } from './product.service';
import { ProductController } from './product.controller';
import { Product } from 'src/typeorm/entities/Product';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductTypeModule } from './product-type/product-type.module';
import { ProductDietCategoryModule } from './product-diet-category/product-diet-category.module';
import { ProductType } from 'src/typeorm/entities/Product_type';
import { ProductDietCategory } from 'src/typeorm/entities/Product_diet_category';
import { Department } from 'src/typeorm/entities/Department';

@Module({
  imports: [
    TypeOrmModule.forFeature([Product, ProductType, ProductDietCategory, Department]),
    ProductTypeModule,
    ProductDietCategoryModule
  ],
  providers: [ProductService],
  controllers: [ProductController]
})
export class ProductModule {}
