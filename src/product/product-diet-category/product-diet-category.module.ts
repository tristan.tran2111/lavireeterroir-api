import { Module } from '@nestjs/common';
import { ProductDietCategoryService } from './product-diet-category.service';
import { ProductDietCategoryController } from './product-diet-category.controller';

@Module({
  providers: [ProductDietCategoryService],
  controllers: [ProductDietCategoryController]
})
export class ProductDietCategoryModule {}
