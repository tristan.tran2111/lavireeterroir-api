import { Controller, Get } from '@nestjs/common';
import { ProductTypeService } from './product-type.service';
import { ApiBadRequestResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('Product-type')
@Controller('product-type')
export class ProductTypeController {

  constructor(private productTypeService: ProductTypeService) { }

  //For getting a list of all product type
  @Get('getProductsType')
  @ApiOperation({ summary: "Get all types" })
  @ApiOkResponse({ description: 'Display all types of the app' })
  @ApiBadRequestResponse({ description: 'Types not found' })
  getProductsType() {
    return this.productTypeService.findAllProductType();
  }
}
