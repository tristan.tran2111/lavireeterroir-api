import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductType } from 'src/typeorm/entities/Product_type';
import { Repository } from 'typeorm';

@Injectable()
export class ProductTypeService {
  constructor(
    @InjectRepository(ProductType) private productTypeRepository: Repository<ProductType>
  ) { }

    findAllProductType() {
      return this.productTypeRepository.find();
    }
}
