import { Module } from '@nestjs/common';
import { ProductTypeService } from './product-type.service';
import { ProductTypeController } from './product-type.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductType } from 'src/typeorm/entities/Product_type';

@Module({
  imports: [
    TypeOrmModule.forFeature([ProductType]),
  ],
  providers: [ProductTypeService],
  controllers: [ProductTypeController]
})
export class ProductTypeModule {}
