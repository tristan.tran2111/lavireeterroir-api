import { ApiProperty } from "@nestjs/swagger";

export class CreateProductDto {
  @ApiProperty()
  product_name: string;

  @ApiProperty()
  product_description: string;

  @ApiProperty()
  product_img: string;

  @ApiProperty()
  ID_product_type: number;

  @ApiProperty()
  ID_product_diet_category: number;
}
