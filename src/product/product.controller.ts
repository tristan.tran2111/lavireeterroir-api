import { BadRequestException, Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ApiBadRequestResponse, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ProductService } from './product.service';
import { CreateProductDto } from './dtos/create-product.dto';
import { Product } from 'src/typeorm/entities/Product';

@ApiTags('Product')
@Controller('product')
export class ProductController {
  constructor(private productService: ProductService) { }

  //For getting a list of all product
  @Get('getProducts')
  @ApiOperation({ summary: "Get all products" })
  @ApiOkResponse({ description: 'Display all products of the app' })
  @ApiBadRequestResponse({ description: 'Products not found' })
  async getProduct() {
    return this.productService.findAllProduct();
  }

  //For getting a department with code
  @Get(':product_name/getProductByName')
  @ApiOperation({ summary: "Get a product by its name" })
  @ApiOkResponse({ description: 'Display product by name' })
  @ApiBadRequestResponse({ description: 'Product not found' })
  async getDepartmentByCode(@Param('product_name') product_name: string): Promise<Product | BadRequestException> {
    try {
      const res = await this.productService.findProductByName(product_name);
      return res
    } catch (error) {
      console.error(error);
      throw new BadRequestException({ message: 'Error fetching product data' });
    }
  }

  // For getting product by department code
  @Get('getProductsByDepartment/:code')
  @ApiOperation({ summary: "Get products by department code" })
  @ApiOkResponse({ description: 'Display products by department code' })
  @ApiBadRequestResponse({ description: 'Department not found' })
  async getProductsByDepartment(@Param('code') code: number) {
    return this.productService.findProductsByDepartmentCode(code);
  }


  // For add a product
  @Post('addProduct')
  @ApiOperation({ summary: "Add a new product" })
  @ApiCreatedResponse({ description: 'The product has been successfully created' })
  async addProduct(@Body() createProductDto: CreateProductDto) {
    try {
      await this.productService.createProduct(createProductDto);
      return { message: 'Product successfully created' };
    } catch (error) {
      console.error(error);
      throw new BadRequestException({ message: 'Error creating product' });
    }
  }
}
