import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from 'src/typeorm/entities/Product';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dtos/create-product.dto';
import { ProductType } from 'src/typeorm/entities/Product_type';
import { ProductDietCategory } from 'src/typeorm/entities/Product_diet_category';
import { Department } from 'src/typeorm/entities/Department';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product) private productRepository: Repository<Product>,
    @InjectRepository(ProductType) private productTypeRepository: Repository<ProductType>,
    @InjectRepository(ProductDietCategory) private productDietCategoryRepository: Repository<ProductDietCategory>,
    @InjectRepository(Department) private departmentRepository: Repository<Department>
  ) { }

    async findAllProduct() {
      return this.productRepository.find({ relations: ['ProductType', 'ProductDietCategory'] });
    }

    async findProductByName(product_name: string) {
      return this.productRepository.findOne({ 
        where: { product_name: product_name }, relations: ['ProductType', 'merchants', 'merchants.photos', 'merchants.owner']
      });
    }

    async createProduct(createProductDto: CreateProductDto) {
      const productType = await this.productTypeRepository.findOne({ where: { ID_product_type: createProductDto.ID_product_type } });
      const productDietCategory = await this.productDietCategoryRepository.findOne({ where: { ID_product_diet_category: createProductDto.ID_product_diet_category } });
  
      if (!productType || !productDietCategory) {
        throw new BadRequestException('Invalid ProductType or ProductDietCategory ID');
      }
  
      const newProduct = this.productRepository.create({
        ...createProductDto,
        ProductType: productType,
        ProductDietCategory: productDietCategory,
      });
  
      return this.productRepository.save(newProduct);
    }

    async findProductsByDepartmentCode(departmentCode: number) {
      const department = await this.departmentRepository.findOne({ where: { department_code: departmentCode }, relations: ['products'] });
      if (!department) {
        throw new BadRequestException('Department not found');
      }
      return department.products;
    }
}
