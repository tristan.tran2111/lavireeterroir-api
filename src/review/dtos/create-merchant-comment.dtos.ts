import { ApiProperty } from '@nestjs/swagger';

export class CreateMerchantCommentDto {
  @ApiProperty()
  content: string;

  @ApiProperty()
  rating: number;
}
