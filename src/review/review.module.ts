import { Module } from '@nestjs/common';
import { ReviewService } from './review.service';
import { ReviewController } from './review.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/typeorm/entities/User';
import { MerchantReview } from 'src/typeorm/entities/MerchantReview';
import { Review } from 'src/typeorm/entities/Review';
import { Merchant } from 'src/typeorm/entities/Merchant';
import { Workshop } from 'src/typeorm/entities/Workshop';

@Module({
  imports: [TypeOrmModule.forFeature([User, MerchantReview, Review, Merchant, Workshop])],
  providers: [ReviewService],
  controllers: [ReviewController]
})
export class ReviewModule {}
