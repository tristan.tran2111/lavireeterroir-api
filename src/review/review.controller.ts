import { Body, Controller, Headers, Param, Post } from '@nestjs/common';
import { ApiBadRequestResponse, ApiCreatedResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ReviewService } from './review.service';
import { CreateMerchantCommentDto } from './dtos/create-merchant-comment.dtos';

@ApiTags('Review')
@Controller('review')
export class ReviewController {
  constructor(
    private readonly reviewService: ReviewService
  ) {}

  @Post(':id')
  @ApiOperation({ summary: 'Create a review for a merchant' })
  @ApiCreatedResponse({ description: 'Review successfully created' })
  @ApiBadRequestResponse({ description: 'Invalid token, user, or merchant' })
  async createReview(
    @Param('id') merchantId: number,
    @Headers('Authorization') authorization: string,
    @Body() createMerchantDto: CreateMerchantCommentDto
  ) {
    const token = authorization.replace('Bearer ', '');
    return await this.reviewService.createMerchantComment(token, merchantId, createMerchantDto);
  }

  @Post(':id/workshop')
  @ApiOperation({ summary: 'Create a review for a workshop' })
  @ApiCreatedResponse({ description: 'Review successfully created' })
  @ApiBadRequestResponse({ description: 'Invalid token, user, or workshop' })
  async createReviewWorkshop(
    @Param('id') workshopId: number,
    @Headers('Authorization') authorization: string,
    @Body() createMerchantDto: CreateMerchantCommentDto
  ) {
    const token = authorization.replace('Bearer ', '');
    return await this.reviewService.createWorkshopComment(token, workshopId, createMerchantDto);
  }
}
