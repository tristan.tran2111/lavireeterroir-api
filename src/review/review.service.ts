import { BadRequestException, Injectable } from '@nestjs/common';
import { MerchantReview } from 'src/typeorm/entities/MerchantReview';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from 'src/typeorm/entities/User';
import { Review } from 'src/typeorm/entities/Review';
import { JwtService } from '@nestjs/jwt';
import { Merchant } from 'src/typeorm/entities/Merchant';
import { CreateMerchantCommentDto } from './dtos/create-merchant-comment.dtos';
import { Workshop } from 'src/typeorm/entities/Workshop';

@Injectable()
export class ReviewService {
  constructor(
    @InjectRepository(User) private userRepository: Repository<User>,
    @InjectRepository(MerchantReview) private merchantReviewRepository: Repository<MerchantReview>,
    @InjectRepository(Review) private reviewRepository: Repository<Review>,
    @InjectRepository(Merchant) private merchantRepository: Repository<Merchant>,
    @InjectRepository(Workshop) private workshopRepository: Repository<Workshop>,
    private jwtService: JwtService
  ) {
  }

  async createMerchantComment(token: string, merchantId: number, createMerchantCommentDetails: CreateMerchantCommentDto) {
    try {
      const decoded = this.jwtService.verify(token);
      const userId = decoded.user_id;

      const user = await this.userRepository.findOne({ where: { ID_user: userId } });
      const merchant = await this.merchantRepository.findOne({ where: { ID_merchant: merchantId } });

      if (!user || !merchant) {
        throw new BadRequestException('Invalid user or merchant');
      }

      const newReview = this.merchantReviewRepository.create({
        review_date: new Date(),
        ...createMerchantCommentDetails,
        user: user,
        merchant: merchant
      });

      await this.merchantReviewRepository.save(newReview);

      return { message: 'Comment Posted' };
    } catch (error) {
      console.error(error);
      throw new BadRequestException('Error creating Review');
    }
  }

  async createWorkshopComment(token: string, workshopId: number, createMerchantCommentDetails: CreateMerchantCommentDto) {
    try {
      const decoded = this.jwtService.verify(token);
      const userId = decoded.user_id;

      const user = await this.userRepository.findOne({ where: { ID_user: userId } });
      const workshop = await this.workshopRepository.findOne({ where: { ID_workshop: workshopId } });

      if (!user || !workshop) {
        throw new BadRequestException('Invalid user or workshop');
      }

      const newReview = this.reviewRepository.create({
        review_date: new Date(),
        ...createMerchantCommentDetails,
        user: user,
        workshop: workshop
      });

      await this.reviewRepository.save(newReview);

      return { message: 'Comment Posted' };
    } catch (error) {
      console.error(error);
      throw new BadRequestException('Error creating Review');
    }
  }
}
