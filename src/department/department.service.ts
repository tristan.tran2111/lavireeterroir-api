import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Department } from 'src/typeorm/entities/Department';
import { Repository } from 'typeorm';
import * as admin from 'firebase-admin';
import { CreateDepartmentDto } from './dtos/create-department.dtos';

@Injectable()
export class DepartmentService {
  constructor(@InjectRepository(Department) private departmentRepository: Repository<Department>) { }

  findAllDepartment() {
    return this.departmentRepository.find({
      select: ['department_name', 'department_img', 'department_code', 'ID_department']
    });
  }

  async findDepartmentByCode(department_code: number) {
    return this.departmentRepository.findOne({
      where: { department_code: department_code }
    });
  }

  async createDepartment(createDepartmentDto: CreateDepartmentDto, file: Express.Multer.File) {
    const imageUrl = await this.uploadFile(file);

    const department = this.departmentRepository.create({
      ...createDepartmentDto,
      department_img: imageUrl,
    });
    
    return this.departmentRepository.save(department);
  }

  async uploadFile(file: Express.Multer.File): Promise<string> {
    const bucket = admin.storage().bucket();
    const fileName = `${Date.now()}-${file.originalname}`;
    const fileUpload = bucket.file(fileName);
    const stream = fileUpload.createWriteStream({
      metadata: {
        contentType: file.mimetype,
      },
    });

    stream.end(file.buffer);

    await new Promise((resolve, reject) => {
      stream.on('finish', resolve);
      stream.on('error', reject);
    });

    const [url] = await fileUpload.getSignedUrl({
      action: 'read',
      expires: '03-01-2500',
    });

    return url;
  }
}
