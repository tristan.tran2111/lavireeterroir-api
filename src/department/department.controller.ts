import { BadRequestException, Body, Controller, Get, Param, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { DepartmentService } from './department.service';
import { ApiBadRequestResponse, ApiBody, ApiConsumes, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Department } from 'src/typeorm/entities/Department';
import { CreateDepartmentDto, CreateDepartmentWithFileDto } from './dtos/create-department.dtos';
import { FileInterceptor } from '@nestjs/platform-express';

@ApiTags('Department')
@Controller('department')
export class DepartmentController {
  constructor(private departmentService: DepartmentService) { }

  //For getting a list of all departments
  @Get('getDepartments')
  @ApiOperation({ summary: "Get all departments" })
  @ApiOkResponse({ description: 'Display all departments of the app' })
  @ApiBadRequestResponse({ description: 'Departments not found' })
  getDepartments() {
    return this.departmentService.findAllDepartment();
  }

  //For getting a department with code
  @Get(':department_code/getDepartmentByCode')
  @ApiOperation({ summary: "Get a department" })
  @ApiOkResponse({ description: 'Display department by department code' })
  @ApiBadRequestResponse({ description: 'Department not found' })
  async getDepartmentByCode(@Param('department_code') departmentCode: number): Promise<Department | BadRequestException> {
    try {
      const res = await this.departmentService.findDepartmentByCode(departmentCode);
      return res
    } catch (error) {
      console.error(error);
      throw new BadRequestException({ message: 'Error fetching department data' });
    }
  }

  @Post('createDepartment')
  @UseInterceptors(FileInterceptor('department_img'))
  @ApiOperation({ summary: "Create a new department" })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'Create Department DTO',
    type: CreateDepartmentWithFileDto,
  })
  @ApiOkResponse({ description: 'Department created successfully' })
  @ApiBadRequestResponse({ description: 'Invalid data' })
  createBlog(@Body() createDepartmentDto: CreateDepartmentDto, @UploadedFile() department_img: Express.Multer.File) {
    return this.departmentService.createDepartment(createDepartmentDto, department_img);
  }
}
