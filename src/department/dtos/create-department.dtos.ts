import { ApiProperty } from '@nestjs/swagger';

export class CreateDepartmentDto {
  @ApiProperty()
  department_name: string;

  @ApiProperty()
  department_description: string;

  @ApiProperty()
  department_code: number;
}

export class CreateDepartmentWithFileDto extends CreateDepartmentDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  department_img: any;
}
