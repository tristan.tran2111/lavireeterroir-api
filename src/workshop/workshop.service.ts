import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Workshop } from 'src/typeorm/entities/Workshop';

@Injectable()
export class WorkshopService {
  constructor(
    @InjectRepository(Workshop)
    private readonly workshopRepository: Repository<Workshop>,
  ) {}

  async findWorkshopById(id: number) {
    const workshop = await this.workshopRepository.findOne({
      where: { ID_workshop: id },
      relations: ['photos', 'organizer', 'organizer.photos', 'reservationDates', 'reviews', 'reviews.user'],
    });
    
    if (!workshop) {
      throw new NotFoundException(`Workshop with ID ${id} not found`);
    }

    const organizerFirstPhoto = workshop.organizer?.photos?.length > 0 ? workshop.organizer.photos[0].url : null;

    return { ...workshop, organizerFirstPhoto };
  }

  async findWorkshopsByDepartment(departmentCode: string) {
    const workshops = await this.workshopRepository
      .createQueryBuilder('workshop')
      .leftJoinAndSelect('workshop.organizer', 'merchant')
      .leftJoinAndSelect('merchant.department', 'department')
      .where('department_code = :departmentCode', { departmentCode })
      .leftJoinAndSelect('workshop.photos', 'photos')
      .leftJoinAndSelect('workshop.reservationDates', 'reservationDates')
      .leftJoinAndSelect('workshop.reviews', 'reviews')
      .leftJoinAndSelect('reviews.user', 'user')
      .getMany();

    if (workshops.length === 0) {
      throw new NotFoundException(`Aucun workshop trouvé pour le département avec le code ${departmentCode}`);
    }

    return workshops;
  }
}
