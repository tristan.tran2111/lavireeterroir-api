import { Module } from '@nestjs/common';
import { WorkshopService } from './workshop.service';
import { WorkshopController } from './workshop.controller';
import { Workshop } from 'src/typeorm/entities/Workshop';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([Workshop])
  ],
  providers: [WorkshopService],
  controllers: [WorkshopController]
})
export class WorkshopModule {}
