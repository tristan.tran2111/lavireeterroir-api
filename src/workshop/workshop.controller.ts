import { Controller, Get, Param } from '@nestjs/common';
import { WorkshopService } from './workshop.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Workshop')
@Controller('workshop')
export class WorkshopController {
  constructor(private readonly workshopService: WorkshopService) {}

  @Get(':id')
  async getWorkshopById(@Param('id') id: number) {
    return this.workshopService.findWorkshopById(id);
  }

  @Get('department/:departmentCode')
  async getWorkshopsByDepartment(@Param('departmentCode') departmentCode: string) {
    return this.workshopService.findWorkshopsByDepartment(departmentCode);
  }
}
