import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Gallery } from 'src/typeorm/entities/Gallery';
import { Merchant } from 'src/typeorm/entities/Merchant';
import { Repository } from 'typeorm';

@Injectable()
export class GalleryService {
  constructor(
    @InjectRepository(Gallery)
    private readonly galleryRepository: Repository<Gallery>,
  ) {}

  async create(url: string, merchant: Merchant): Promise<Gallery> {
    const newGallery = this.galleryRepository.create({ url, merchant });
    return this.galleryRepository.save(newGallery);
  }

  async findAll(): Promise<Gallery[]> {
    return this.galleryRepository.find();
  }
}
