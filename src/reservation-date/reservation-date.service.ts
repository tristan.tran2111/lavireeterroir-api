import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ReservationDate } from 'src/typeorm/entities/Reservation-Date';
import { Repository } from 'typeorm';

@Injectable()
export class ReservationDateService {
  constructor(
    @InjectRepository(ReservationDate)
    private reservationDateRepository: Repository<ReservationDate>,
  ) {}

  async findById(id: number): Promise<ReservationDate> {
    return await this.reservationDateRepository.findOne({ where: { ID_reservation_date: id }, relations: ['workshop', 'workshop.photos'] });
  }
}
