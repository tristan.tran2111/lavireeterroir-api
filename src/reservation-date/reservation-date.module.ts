import { Module } from '@nestjs/common';
import { ReservationDateController } from './reservation-date.controller';
import { ReservationDateService } from './reservation-date.service';
import { ReservationDate } from 'src/typeorm/entities/Reservation-Date';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([ReservationDate])],
  providers: [ReservationDateService],
  controllers: [ReservationDateController]
})
export class ReservationDateModule {}
