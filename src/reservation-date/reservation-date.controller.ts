import { Controller, Get, Param } from '@nestjs/common';
import { ReservationDateService } from './reservation-date.service';
import { ReservationDate } from 'src/typeorm/entities/Reservation-Date';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Reservation')
@Controller('reservation-dates')
export class ReservationDateController {
  constructor(private readonly reservationDateService: ReservationDateService) {}

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<ReservationDate> {
    return this.reservationDateService.findById(id);
  }
}
