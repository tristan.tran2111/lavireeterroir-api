import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Booking } from 'src/typeorm/entities/Booking';
import { ReservationDate } from 'src/typeorm/entities/Reservation-Date';
import { User } from 'src/typeorm/entities/User';
import { Workshop } from 'src/typeorm/entities/Workshop';
import { Repository } from 'typeorm';
import { CreateBookingDto } from './dtos/booking.dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class BookingService {
    constructor(
        @InjectRepository(Booking)
        private bookingRepository: Repository<Booking>,
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(Workshop)
        private workshopRepository: Repository<Workshop>,
        @InjectRepository(ReservationDate)
        private reservationDateRepository: Repository<ReservationDate>,
        private jwtService: JwtService,
    ) {}

    async create(token: string, createBookingDto: CreateBookingDto) {
        const { workshopId, reservationDateId, booking_date, number_of_persons, total_price } = createBookingDto;

        let user;
        try {
            const decodedToken = this.jwtService.verify(token);
            console.log('Decoded token:', decodedToken);

            if (!decodedToken || !decodedToken.user_id) {
                throw new BadRequestException('Token does not contain user_id');
            }
            
            user = await this.userRepository.findOne({
                where: { ID_user: decodedToken.user_id }
            });
            
            if (!user) {
                throw new NotFoundException(`User with ID ${decodedToken.user_id} not found`);
            }
            
            console.log(`Found user: ${user.ID_user}`);
        } catch (error) {
            console.error('Token verification error:', error);
            throw new BadRequestException('Invalid token');
        }

        const workshop = await this.workshopRepository.findOne({ where: { ID_workshop: workshopId } });
        if (!workshop) throw new NotFoundException(`Workshop with ID ${workshopId} not found`);
        console.log(`Found workshop: ${workshop.ID_workshop}`);

        const reservationDate = await this.reservationDateRepository.findOne({ where: { ID_reservation_date: reservationDateId } });
        if (!reservationDate) throw new NotFoundException(`Reservation date with ID ${reservationDateId} not found`);
        console.log(`Found reservation date: ${reservationDate.ID_reservation_date}`);

        if (reservationDate.available_places < number_of_persons) {
            throw new NotFoundException('Not enough available places');
        }

        reservationDate.available_places -= number_of_persons;
        await this.reservationDateRepository.save(reservationDate);

        const booking = new Booking();
        booking.booking_date = booking_date;
        booking.number_of_persons = number_of_persons;
        booking.total_price = total_price;
        booking.user = user;
        booking.workshop = workshop;
        booking.reservationDate = reservationDate;

        const savedBooking = await this.bookingRepository.save(booking);

        return savedBooking;
    }

    async deleteBooking(token: string, bookingId: number) {
        let user;
        try {
            const decodedToken = this.jwtService.verify(token);

            if (!decodedToken || !decodedToken.user_id) {
                throw new BadRequestException('Token does not contain user_id');
            }
            
            user = await this.userRepository.findOne({
                where: { ID_user: decodedToken.user_id }
            });
            
            if (!user) {
                throw new NotFoundException(`User with ID ${decodedToken.user_id} not found`);
            }
            
        } catch (error) {
            console.error('Token verification error:', error);
            throw new BadRequestException('Invalid token');
        }

        const booking = await this.bookingRepository.findOne({
            where: { ID_booking: bookingId, user: { ID_user: user.ID_user } },
            relations: ['reservationDate']
        });

        if (!booking) {
            throw new NotFoundException(`Booking with ID ${bookingId} not found`);
        }

        // Increase the available places of the reservation date
        booking.reservationDate.available_places += booking.number_of_persons;
        await this.reservationDateRepository.save(booking.reservationDate);

        await this.bookingRepository.remove(booking);
        console.log(`Deleted booking with ID: ${bookingId}`);

        return { message: `Booking with ID ${bookingId} has been deleted` };
    }
}
