import { Controller, Post, Body, Param, Delete } from '@nestjs/common';
import { BookingService } from './booking.service';
import { ApiBadRequestResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CreateBookingDto } from './dtos/booking.dto';

@ApiTags('Bookings')
@Controller('bookings')
export class BookingController {
    constructor(private readonly bookingService: BookingService) {}

    @Post(':token')
    async create(@Param('token') token: string, @Body() createBookingDto: CreateBookingDto) {
        return this.bookingService.create(token, createBookingDto);
    }

    @Delete(':bookingId/:token')
    @ApiOperation({ summary: 'Delete a booking' })
    @ApiOkResponse({ description: 'Booking deleted successfully' })
    @ApiBadRequestResponse({ description: 'Error deleting booking' })
    async deleteBooking(@Param('token') token: string, @Param('bookingId') bookingId: number) {
        return await this.bookingService.deleteBooking(token, bookingId);
    }
}
