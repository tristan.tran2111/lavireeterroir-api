import { Module } from '@nestjs/common';
import { BookingController } from './booking.controller';
import { BookingService } from './booking.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Booking } from 'src/typeorm/entities/Booking';
import { User } from 'src/typeorm/entities/User';
import { Workshop } from 'src/typeorm/entities/Workshop';
import { ReservationDate } from 'src/typeorm/entities/Reservation-Date';

@Module({
  imports: [TypeOrmModule.forFeature([Booking, User, Workshop, ReservationDate])],
  providers: [BookingService],
  controllers: [BookingController]
})
export class BookingModule {}
