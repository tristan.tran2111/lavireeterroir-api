import { ApiProperty } from '@nestjs/swagger';

export class CreateBookingDto {
    @ApiProperty()
    workshopId: number;

    @ApiProperty()
    reservationDateId: number;

    @ApiProperty()
    booking_date: Date;

    @ApiProperty()
    number_of_persons: number;

    @ApiProperty()
    total_price: number;
}
